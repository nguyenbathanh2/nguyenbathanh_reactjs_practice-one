# ReactJS Practice One

## REQUIREMENTS

- Mocking data list of products.
  - Render the list of products.
  - The user can add a new product by name and image URL - Open the form modal to add a new product.
- Delete the product in the list.
  - The user can delete a product by clicking the Remove button - show confirmation popup.
- Search product by name:
  - The user can search for the product by inputting the name of the product into the Search Bar.

## ESTIMATION DETAILS

- > [Here](https://docs.google.com/document/d/1YLLZyT5REFpwoHEPJz47pL3CzOwtY69w/edit#)

##How to run the project

_Step one:_ Clone the code folder from gitlab to your device

- Choose a path to save that file -> At that path open the command window
- Run command
  > git clone -b feature/react-practice-one https://gitlab.com/nguyenbathanh2/Internship.git

_Step two:_ Run project

- cd project

- Turn on Visual Studio Code. Open the folder you just cloned to your computer.

- Install the npm dependencies

  > npm install

- Run start server

  > npm run server

- Run start project

  > npm start

- Hold ctrl and click on the successfully created localhost link to view the website
