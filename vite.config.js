/* eslint-disable import/no-extraneous-dependencies */

import react from '@vitejs/plugin-react';
import path from 'node:path';
import { defineConfig } from 'vite';

// https://vitejs.dev/config/
export default defineConfig({
  resolve: {
    alias: {
      '@actions': path.resolve(__dirname, './src/actions'),
      '@components': path.resolve(
        __dirname,
        './src/components'
      ),
      '@constants': path.resolve(
        __dirname,
        './src/constants'
      ),
      '@data': path.resolve(__dirname, './src/data'),
      '@helpers': path.resolve(__dirname, './src/helpers'),
      '@store': path.resolve(__dirname, './src/store'),
    },
  },
  plugins: [react()],
});
