// Constants ----------------------------------------------------------------
import { API, BASE_URL } from '@constants/constants';

// Get data by GET method
export const getAllProduct = async (url) => {
  const res = await fetch(`${BASE_URL}${API}${url}/`);
  if (res.ok) {
    return res.json();
  }
  throw new Error(
    'Failed to load resource: the server responded with a status of '
      + res.status,
    res.statusText
  );
};

// Post data by POST method
export const postNewProduct = async (url, data) => {
  const res = await fetch(`${BASE_URL}${url}`, {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
    },
    body: JSON.stringify(data),
  });
  if (res.ok) {
    return res.json();
  }
  throw new Error(
    res.status,
    res.statusText,
    'pass in the solution how to correct it'
  );
};

// Delete data by DELETE method
export const deleteTheProduct = async (url) => {
  const res = await fetch(`${BASE_URL}${API}/${url}`, {
    method: 'DELETE',
  });
  if (res.ok) {
    return res.json();
  }
  throw new Error(
    res.status,
    res.statusText,
    'pass in the solution how to correct it'
  );
};
