//Libraries
import React from 'react';
import ReactDOM from 'react-dom/client';

// store
import Provider from '@store/Provider';

//App
import App from './App';

//Style
import './index.css';

ReactDOM.createRoot(document.getElementById('root')).render(
  <React.StrictMode>
    <Provider>
      <App />
    </Provider>
  </React.StrictMode>
);
