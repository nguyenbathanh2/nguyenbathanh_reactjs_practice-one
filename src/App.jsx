// Libraries
import React from 'react';

// Components
import Navigation from '@components/Navigation/Navigation';
import ProductMaker from '@components/ProductMaker/ProductMaker';

// Style
import './App.css';

const App = () => {
  return (
    <div className="App">
      <Navigation />
      <ProductMaker />
    </div>
  );
};

export default App;
