// Libraries
import React from 'react';

// Components
import Button from '@components/Button/Button';

// Actions

// Style
import './productItem.css';

const ProductItem = ({ id, image, title, showPopup }) => {
  // Function to handle when the user clicks on Delete button
  const handleClickDeleteItem = () => {
    showPopup(id);
  };

  return (
    <div className="content-wrapper">
      <h3>{title}</h3>
      <Button
        className="delete-button"
        onClick={handleClickDeleteItem}
        text="X"
      />
      <img className="item" src={image} />
    </div>
  );
};
export default ProductItem;
