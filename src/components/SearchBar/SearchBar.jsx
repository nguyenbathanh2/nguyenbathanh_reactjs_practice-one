// Libraries
import React, { useState } from 'react';

// Components
import FormInput from '@components/FormInput/FormInput';

// Style
import './search.css';

const SearchBar = ({ onSearch }) => {
  const [searchInput, setSearchInput] = useState('');

  // Function to handle changes in the search input
  const handleSearchInputChange = (e) => {
    setSearchInput(e.target.value);
    onSearch(e.target.value);
  };

  return (
    <>
      <label htmlFor="searchbar" />
      <FormInput
        className="search"
        type="search"
        name="search"
        value={searchInput}
        onChange={(e) => handleSearchInputChange(e)}
        placeholder="search.."
      />
    </>
  );
};

export default SearchBar;
