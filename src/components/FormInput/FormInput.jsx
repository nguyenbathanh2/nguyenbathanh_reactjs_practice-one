// Libraries
import React from 'react';

const FormInput = ({
  onChange,
  value,
  type,
  className,
  placeholder,
}) => {
  return (
    <input
      onChange={onChange}
      value={value}
      className={className}
      placeholder={placeholder}
      type={type}
    />
  );
};

export default FormInput;
