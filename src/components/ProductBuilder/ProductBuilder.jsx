// Libraries
import React from 'react';

// Components
import ProductItem from '@components/ProductItem/ProductItem';

// Style
import './productBuider.css';

const ProductBuilder = ({ showPopup, allProductItems }) => {
  return (
    <div className="builder">
      {allProductItems?.map((productData) => (
        <ProductItem
          key={productData.id}
          image={productData.image}
          title={productData.title}
          id={productData.id}
          showPopup={showPopup}
        />
      ))}
    </div>
  );
};

export default ProductBuilder;
