// Libraries
import React, { useEffect, useState } from 'react';
import validator from 'validator';

// Actions

// Components
import Button from '@components/Button/Button';
import FormInput from '@components/FormInput/FormInput';

// Style
import './modal.css';

const ModalProduct = ({ handleClose, show, onAdd }) => {
  const [error, setError] = useState('');
  const [title, setTitle] = useState('');
  const [image, setImage] = useState('');
  const [isValid, setIsValid] = useState('');

  // Function to handle input title
  const handleChangeTitle = (e) => {
    setTitle(e.target.value);
  };

  // function to handle input url image
  const handleChangeImage = (e) => {
    if (validator.isURL(e.target.value)) {
      setImage(e.target.value);
      setIsValid('Is Valid URL');
    } else {
      setIsValid('Is Not Valid URL');
    }
  };

  // Function to handle submit and validation errors
  const handleClickSubmitForm = async () => {
    if (title && image) {
      // If there is enough information, create a new item
      const newItem = { title, image };
      if (onAdd) {
        onAdd(newItem);
      }
      handleClose();
      setTitle();
      setImage();
      setIsValid();
    } else {
      setError('Please enter full information');
    }
  };

  // Revert the validation state
  useEffect(() => {
    setError('');
  }, [show]);

  return (
    show && (
      <div id="modal" className="modal">
        <div className="modal-content">
          <div className="modal-header">
            <h2 className="modal-title">
              Add a new product
            </h2>
            <Button
              className="close-button"
              onClick={handleClose}
              text={<i className="fas fa-times fa-lg" />}
            />
          </div>
          <div className="modal-body">
            <form>
              <div className="form-group">
                <label
                  className="form-label"
                  htmlFor="title"
                >
                  Title
                </label>
                <FormInput
                  className="form-input"
                  type="text"
                  name="title"
                  required
                  maxLength="100"
                  value={title}
                  onChange={(e) => handleChangeTitle(e)}
                />
              </div>
              <div className="form-group">
                <label
                  className="form-label"
                  htmlFor="image"
                >
                  image
                </label>
                <FormInput
                  className="form-input form-input-image"
                  type="URL"
                  name="URL"
                  required
                  maxLength="200"
                  value={image}
                  onChange={handleChangeImage}
                />
                <span className="data-fields-empty">
                  {isValid}
                </span>
              </div>
            </form>
          </div>
          <div className="modal-footer">
            <Button
              className="button-submit"
              text="Add"
              onClick={handleClickSubmitForm}
            />
          </div>
          <p className="data-fields-empty">{error}</p>
        </div>
      </div>
    )
  );
};

export default ModalProduct;
