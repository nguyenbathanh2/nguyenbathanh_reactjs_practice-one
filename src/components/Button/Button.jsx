// Libraries
import React from 'react';

// Style
import './style.css';

// function button component
const Button = ({ onClick, text, className }) => {
  return (
    <button onClick={onClick} className={className}>
      {text}
    </button>
  );
};

export default Button;
