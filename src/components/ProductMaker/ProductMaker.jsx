// Libraries
import React, {
  useContext,
  useEffect,
  useState,
} from 'react';

// Components
import Button from '@components/Button/Button';
import ConfirmPopup from '@components/ConfirmPopup/ConfirmPopup';
import ModalProduct from '@components/Modal/Modal';
import ProductBuilder from '@components/ProductBuilder/ProductBuilder';
import SearchBar from '@components/SearchBar/SearchBar';

// data
import { API } from '@constants/constants';

// Helpers
import {
  deleteTheProduct,
  getAllProduct,
  postNewProduct,
} from '@helpers/services';

//store
import { Context } from '@store/Context';

// Style
import './productMaker.css';

const ProductMaker = () => {
  const [product, setProduct] = useContext(Context);
  const [showModal, setShowModal] = useState(false);
  const [showPopup, setShowPopup] = useState(false);
  const [idSeleted, setIdSeleted] = useState('');
  const [error, setError] = useState('');

  // Function to handle when the user clicks on Add button
  const handleClickAdd = () => {
    setShowModal(true);
  };

  // Start loading the page, then call the API to get the data and assign it to the state
  const fetchProduct = async () => {
    try {
      const data = await getAllProduct('');
      setProduct(data);
      //return data when searching for products
      return data;
    } catch (e) {
      setError(e.message);
    }
  };

  // Function to create the new product
  const handleAdd = async (item) => {
    if (item.title && item.image) {
      try {
        // Add -> call API, add information of product to db.json then update state again
        postNewProduct(`${API}`, item).then((res) => {
          if (res) {
            fetchProduct();
          }
        });
      } catch (e) {
        setError(e.message);
      }
    }
  };

  // Function to handle delete the product
  const handleDelete = async () => {
    if (idSeleted) {
      try {
        const data = await deleteTheProduct(idSeleted);
        if (data) {
          fetchProduct();
        }
      } catch (e) {
        setError(e.message);
      }
    }
    setShowPopup(false);
  };

  // function
  const handleShowPopup = (id) => {
    setShowPopup(true);
    setIdSeleted(id);
  };

  // Function to handle search the product
  const handleSearch = async (searchInput) => {
    if (searchInput) {
      let data = await fetchProduct();
      data = data.filter((el) =>
        el.title
          .toLowerCase()
          .includes(searchInput.trim().toLowerCase())
      );
      setProduct(data);
    } else {
      fetchProduct();
    }
  };

  useEffect(() => {
    fetchProduct();
  }, []);

  return (
    <div>
      <h1 className="content">
        <span role="img" aria-label="product">
          🥗{' '}
        </span>
        Build Your Custom Salad!
        <span role="img" aria-label="product">
          {' '}
          🥗
        </span>
      </h1>
      <SearchBar onSearch={handleSearch} />
      <Button
        id="btn-add"
        className="btn-add"
        text="+ Add"
        onClick={handleClickAdd}
      />
      {!!error && (
        <span className="error-api"> {error} </span>
      )}
      {!error && (
        <ProductBuilder
          allProductItems={product}
          showPopup={handleShowPopup}
        />
      )}
      <ModalProduct
        handleClose={() => setShowModal(false)}
        show={showModal}
        onAdd={handleAdd}
      />
      <ConfirmPopup
        openConfirmPopup={showPopup}
        titleConfirm="CONFIRM"
        messageConfirm="Are you sure you want to delete this product ?"
        onConfirm={handleDelete}
        onCancel={() => setShowPopup(false)}
      />
    </div>
  );
};
export default ProductMaker;
