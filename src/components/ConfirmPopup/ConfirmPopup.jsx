// Libraries
import React from 'react';

// Components
import Button from '@components/Button/Button';

// Style
import './confirmPopup.css';

const ConfirmPopup = ({
  onCancel,
  onConfirm,
  openConfirmPopup,
  titleConfirm,
  messageConfirm,
}) => {
  return (
    openConfirmPopup && (
      <div className="confirm-alert-overlay">
        <div className="confirm-alert">
          <div className="confirm-alert-body">
            <i className="far fa-question-circle" />
            <span className="title-confirm">
              {titleConfirm}
            </span>
            <span className="message-confirm">
              {messageConfirm}
            </span>
            <div className="confirm-alert-button-group">
              <Button
                onClick={onConfirm}
                className="confirm-alert-button-confirm"
                text="CONFIRM"
              />
              <Button
                onClick={onCancel}
                className="confirm-alert-button-cancel"
                text="CANCEL"
              />
            </div>
          </div>
        </div>
      </div>
    )
  );
};
export default ConfirmPopup;
