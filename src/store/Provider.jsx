import { Context } from '@store/Context';
import React, { useState } from 'react';

//use Component Provider cover App
//-> can use state in App and components in App
const Provider = ({ children }) => {
  const [product, setProduct] = useState([]);
  return (
    <Context.Provider value={[product, setProduct]}>{children}</Context.Provider>
  );
};

export default Provider;
